﻿namespace _0ccJsonParser
{
    public class Metadata
    {
        public string artist { get; set; }
        public string comment { get; set; }
        public string copyright { get; set; }
        public bool show_comment_on_open { get; set; }
        public string title { get; set; }
    }
}