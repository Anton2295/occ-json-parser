﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0ccJsonParser
{
    public class RootObject
    {
        public List<Channel> channels { get; set; }
        public List<object> detunes { get; set; }
        public List<object> dpcm_samples { get; set; }
        public Global global { get; set; }
        public List<object> grooves { get; set; }
        public List<Instrument> instruments { get; set; }
        public Metadata metadata { get; set; }
        public List<Sequence> sequences { get; set; }
        public List<Song> songs { get; set; }
    }


}
