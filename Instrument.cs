﻿using System.Collections.Generic;

namespace _0ccJsonParser
{
    public class Instrument
    {
        public string chip { get; set; }
        public List<object> dpcm_map { get; set; }
        public int index { get; set; }
        public string name { get; set; }
        public List<SequenceFlag> sequence_flags
        {
            get; set;
        }
    }
}
