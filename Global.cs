﻿namespace _0ccJsonParser
{
    public class Global
    {
        public Detune detune { get; set; }
        public int engine_speed { get; set; }
        public int fxx_split_point { get; set; }
        public bool linear_pitch { get; set; }
        public string machine { get; set; }
        public string vibrato_style { get; set; }
    }
}