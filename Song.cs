﻿using System.Collections.Generic;

namespace _0ccJsonParser
{
    public class Song
    {
        public List<object> bookmarks { get; set; }
        public int frames { get; set; }
        public List<int> highlight { get; set; }
        public int rows { get; set; }
        public int speed { get; set; }
        public int tempo { get; set; }
        public string title { get; set; }
        public List<Track> tracks { get; set; }
        public bool uses_groove { get; set; }
    }
}