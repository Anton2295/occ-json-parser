﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0ccJsonParser
{
    public class SequenceFlag
    {
        public int macro_id { get; set; }
        public int seq_index { get; set; }
    }
}
