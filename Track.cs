﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0ccJsonParser
{
    public class Track
    {
        public string chip { get; set; }
        public int effect_columns { get; set; }
        public List<int> frame_list { get; set; }
        public List<Pattern> patterns { get; set; }
        public int subindex { get; set; }
    }
}
