﻿using System.Collections.Generic;

namespace _0ccJsonParser
{
    public class Sequence
    {
        public string chip { get; set; }
        public int index { get; set; }
        public List<int> items { get; set; }
        public int macro_id { get; set; }
        public int setting_id { get; set; }
        public int loop { get; set; } = 255;
        public int release { get; set; } = 255;
    }

}