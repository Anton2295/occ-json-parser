﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0ccJsonParser
{
    public class Pattern
    {
        public int index { get; set; }
        public List<NoteElement>  notes { get; set; }
    }
}
