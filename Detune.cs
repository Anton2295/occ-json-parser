﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _0ccJsonParser
{
    public class Detune
    {
        public int cents { get; set; }
        public int semitones { get; set; }
    }
}
