﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace _0ccJsonParser
{
    class Program
    {
        static void Main(string[] args)
        {
            string fileName = "Duck_Tales_Moon.json";

            string jSon = System.IO.File.ReadAllText(fileName).Replace("\n", " ");


            RootObject rootObject = JsonConvert.DeserializeObject<RootObject>(jSon);


            Console.WriteLine("const char tempo ={0};", rootObject.songs.ToArray()[0].tempo);
            Console.WriteLine("const char speed ={0};", rootObject.songs.ToArray()[0].speed);
                  

            Console.WriteLine("-------------------------------------");

            for(int i = 0;i < rootObject.sequences.Count;i++ )
            {
                var sequence = rootObject.sequences[i];

                List<string> list = new List<string> { sequence.items.Count.ToString(), sequence.loop.ToString(), sequence.release.ToString(), addQuotes(sequence.items) };

                Console.WriteLine("const struct Data sequence{0} = {1};", i , addQuotes(list));

            }

            Console.WriteLine("-------------------------------------");

            List<string> instrumentList = new List<string>();

            foreach (var instrument in rootObject.instruments)
            {
                List<string> list = new List<string>();
               //Volume
                var i  = instrument.sequence_flags.Where<SequenceFlag>(q => q.macro_id == 0);
                if (i.Count() > 0)
                {

                    list.Add("&sequence" + i.First().seq_index);
                }
                else
                    list.Add("&sequenceNO");

                //Arpeggio
                i = instrument.sequence_flags.Where<SequenceFlag>(q => q.macro_id == 1);
                if (i.Count() > 0)
                {

                    list.Add("&sequence" + i.First().seq_index);
                }
                else
                    list.Add("&sequenceNO");
                // Duty
                i = instrument.sequence_flags.Where<SequenceFlag>(q => q.macro_id == 4);
                if (i.Count() > 0)
                {

                    list.Add("&sequence" + i.First().seq_index);
                }
                else
                    list.Add("&sequenceNO");

                instrumentList.Add(addQuotes(list));



            }

            Console.WriteLine("--------");



            Console.WriteLine("struct Instrument instruments[] = {0};" , addQuotes(instrumentList,true));

            Console.WriteLine("const char instrumentCout ={0};", rootObject.instruments.Count);

            Console.WriteLine("-------------------------------------");

            List<string> frameList = new List<string>();

            var tracks = rootObject.songs.First().tracks;

            foreach (var track in tracks)
            {
                frameList.Add(addQuotes(track.frame_list));
            }

            Console.WriteLine("const char frame[][] = {0};" , addQuotes(frameList,true));



            
            

            /*
            var tracks = rootObject.songs.First().tracks;

            int maxFrame_listCont = tracks.Max(q => q.frame_list.Count);

            for (int i = 0; i < maxFrame_listCont; i++)
            {
                List<int> list = new List<int>();

                for (int nomberTrack = 0; nomberTrack < tracks.Count; nomberTrack++)
                {
                    list.Add(tracks[nomberTrack].frame_list[i]);
                }
                Console.WriteLine(addQuotes(list));
            }

            Console.WriteLine("-------------------------------------");

            */
           

            foreach (var track in tracks)
            {


                for(int nomberPattern = 0; nomberPattern < track.patterns.Count; nomberPattern++)
                {
                    List<string> list = new List<string>();
                    var pattern = track.patterns[nomberPattern];

               //     Console.WriteLine("");
                    Console.WriteLine("//Pattern"+ nomberPattern);

                    for (int i = 0; i < rootObject.songs.First().rows; i++)
                    {
                        var rows = pattern.notes.Where(q => q.row == i);
                    if (rows.Count() > 0)
                        {
                            list.Add(addQuotes(new List<int>() { rows.First().row, rows.First().note.value, rows.First().note.inst_index, rows.First().note.volume }));
                        }
                        else
                            list.Add("{0,0,0,0}");

                    }

                    Console.WriteLine(addQuotes(list,true));
                }

                /*

                foreach (var pattern in track.patterns)
                {

                    foreach (var note in pattern.notes)
                    {
                        Console.WriteLine(addQuotes(new List<int>() { note.row , note.note.value, note.note.inst_index, note.note.volume }));
                    }

                    Console.WriteLine("----------");

                }

                 */

            }

            Console.WriteLine("-------------------------------------");

            Console.ReadLine();
        }


        static string addQuotes(IEnumerable enumerable)
        {
            return addQuotes(enumerable,false);
        }


        static string addQuotes(IEnumerable enumerable, bool isWrap)
        {
            string rez = "";

            rez += '{';
            foreach (var enumer in enumerable)
            {
                rez += enumer.ToString();
                rez += ',';

                if(isWrap) rez += '\n';
            }
            rez =  rez.Remove(rez.Length -1);
            if (isWrap) rez = rez.Remove(rez.Length - 1);

            rez += '}';

            return rez;
        }
    } 
}
